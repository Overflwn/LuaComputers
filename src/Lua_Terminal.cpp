/**
 * @file Lua_Terminal.cpp
 * @author Overflwn (overflwn@gmail.com)
 * @brief (See Lua_Terminal.hpp)
 * @version 0.1
 * @date 2019-11-10
 * 
 * @copyright Copyright (c) 2019
 * 
 */

#include "Lua_Terminal.hpp"
#include "Terminal.hpp"
#include "Color.hpp"

luacomputers::Terminal* getTerminal(lua_State* L)
{
    lua_pushstring(L, "terminal");
    lua_gettable(L, LUA_REGISTRYINDEX);
    void* val = lua_touserdata(L, -1);
    lua_pop(L, 1);
    return (luacomputers::Terminal*)val;
}

int luacomputers::lua::terminal::setPixel(lua_State* L)
{
    int n = lua_gettop(L);
    if(n != 3)
    {
        //More or less than 3 arguments.
        lua_pushboolean(L, false);
        lua_pushstring(L, "Invalid number of arguments. (3 expected)");
        return 2;
    }
    int x = lua_tointeger(L, 1);
    int y = lua_tointeger(L, 2);
    uint16_t col = lua_tointeger(L, 3);
    luacomputers::Terminal* val = getTerminal(L);
    if(val == nullptr) 
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "LuaComputers error: Couldn't get terminal pointer.");
        return 2;
    }else
    {
        val->setPixel(x, y, col);
        return 0;
    }
}

int luacomputers::lua::terminal::getPixel(lua_State* L)
{
    int n = lua_gettop(L);
    if(n != 2)
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Invalid number of arguments. (2 expected)");
        return 2;
    }
    int x = lua_tointeger(L, 1);
    int y = lua_tointeger(L, 2);
    luacomputers::Terminal* val = getTerminal(L);
    if(val == nullptr) 
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "LuaComputers error: Couldn't get terminal pointer.");
        return 2;
    }else
    {
        uint16_t col = val->getPixel(x, y);
        lua_pushnumber(L, col);
        return 1;
    }
}

int luacomputers::lua::terminal::setPaletteColor(lua_State* L)
{
    int n = lua_gettop(L);
    if(n != 4)
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Invalid number of arguments. (4 expected)");
        return 2;
    }
    uint16_t index = lua_tointeger(L, 1);
    uint16_t r = lua_tointeger(L, 2);
    uint16_t g = lua_tointeger(L, 3);
    uint16_t b = lua_tointeger(L, 4);
    luacomputers::Terminal* val = getTerminal(L);
    if(val == nullptr) 
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "LuaComputers error: Couldn't get terminal pointer.");
        return 2;
    }else
    {
        val->setPaletteColor(index, {r, g, b});
        lua_pushboolean(L, true);
        return 1;
    }
}

int luacomputers::lua::terminal::getPaletteColor(lua_State* L)
{
    int n = lua_gettop(L);
    if(n != 1)
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Invalid number of arguments. (1 expected)");
        return 2;
    }
    uint16_t index = lua_tointeger(L, 1);
    luacomputers::Terminal* val = getTerminal(L);
    if(val == nullptr) 
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "LuaComputers error: Couldn't get terminal pointer.");
        return 2;
    }else
    {
        luacomputers::Color col = val->getPaletteColor(index);
        lua_pushinteger(L, col.r);
        lua_pushinteger(L, col.g);
        lua_pushinteger(L, col.b);
        return 3;
    }
}

int luacomputers::lua::terminal::fillRect(lua_State* L)
{
    int n = lua_gettop(L);
    if(n != 5)
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Invalid number of arguments. (5 expected)");
        return 2;
    }
    unsigned int x = lua_tointeger(L, 1);
    unsigned int y = lua_tointeger(L, 2);
    unsigned int w = lua_tointeger(L, 3);
    unsigned int h = lua_tointeger(L, 4);
    uint16_t col = lua_tointeger(L, 5);
    luacomputers::Terminal* val = getTerminal(L);
    if(val == nullptr) 
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "LuaComputers error: Couldn't get terminal pointer.");
        return 2;
    }else
    {
        val->fillRect(x, y, w, h, col);
        return 0;
    }
}
int luacomputers::lua::terminal::clear(lua_State* L)
{
    int n = lua_gettop(L);
    if(n != 1)
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Invalid number of arguments. (1 expected)");
        return 2;
    }
    uint16_t col = lua_tointeger(L, 1);
    luacomputers::Terminal* val = getTerminal(L);
    if(val == nullptr) 
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "LuaComputers error: Couldn't get terminal pointer.");
        return 2;
    }else
    {
        val->clear(col);
        return 0;
    }
}

uint16_t** getTable(lua_State* L, const size_t width, const size_t height, const int indx)
{
    //TODO: Threads lock up or crash for some reason
    uint16_t** data = new uint16_t*[height];
    for(int i=0; i<height; i++)
    {
        data[i] = new uint16_t[width];
    }
    lua_pushnil(L); //first key
    int y=0;
    int x=0;
    while(lua_next(L, indx) != 0)
    {
        // key at index -2 and value at index -1
        //////////2nd dimension
        lua_pushnil(L);
        while(lua_next(L, -2) != 0)
        {
            uint16_t num = lua_tointeger(L, -1);
            data[y][x] = num;
            x++;
            lua_pop(L, 1);
        }
        //////////end 2nd dimension
        //remove value
        lua_pop(L,1);

        x = 0;
        y++;
    }
    return data;
}

//data, x, y, width, height
int luacomputers::lua::terminal::flushBuffer(lua_State* L)
{
    int n = lua_gettop(L);
    if(n != 5)
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Invalid number of arguments. (5 expected)");
        return 2;
    }
    unsigned int x = lua_tointeger(L, 2);
    unsigned int y = lua_tointeger(L, 3);
    size_t width = lua_tointeger(L, 4);
    size_t height = lua_tointeger(L, 5);
    uint16_t** col = getTable(L, width, height, 1);
    luacomputers::Terminal* val = getTerminal(L);
    if(val == nullptr) 
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "LuaComputers error: Couldn't get terminal pointer.");
        return 2;
    }else
    {
        val->flushBuffer(x, y, width, height, col);
        return 0;
    }
}

int luacomputers::lua::terminal::getWidth(lua_State* L)
{
    luacomputers::Terminal* val = getTerminal(L);
    if(val == nullptr) 
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "LuaComputers error: Couldn't get terminal pointer.");
        return 2;
    }else
    {
        lua_pushinteger(L, val->getTermWidth());
        return 1;
    }
}

int luacomputers::lua::terminal::getHeight(lua_State* L)
{
    luacomputers::Terminal* val = getTerminal(L);
    if(val == nullptr) 
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "LuaComputers error: Couldn't get terminal pointer.");
        return 2;
    }else
    {
        lua_pushinteger(L, val->getTermHeight());
        return 1;
    }
}