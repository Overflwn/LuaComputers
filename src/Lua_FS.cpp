#include "Lua_FS.hpp"
#include <boost/range/iterator_range.hpp>
#include <boost/filesystem.hpp>
#include <string>
#include <vector>
#include "Debug.hpp"

int luacomputers::lua::filesystem::makeDir(lua_State* L)
{
    int n = lua_gettop(L);
    if(n != 1)
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Invalid number of arguments. (1 expected)");
        return 2;
    }
    std::string path = lua_tostring(L, -1);
    if(boost::filesystem::create_directories(path))
    {
        lua_pushboolean(L, true);
    }else
    {
        lua_pushboolean(L, false);
    }
    return 1;
}

int luacomputers::lua::filesystem::exists(lua_State* L)
{
    int n = lua_gettop(L);
    if(n != 1)
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Invalid number of arguments. (1 expected)");
        return 2;
    }
    std::string path = lua_tostring(L, -1);
    if(boost::filesystem::exists(path))
    {
        lua_pushboolean(L, true);
    }else
    {
        lua_pushboolean(L, false);
    }
    return 1;
}

int luacomputers::lua::filesystem::isDirectory(lua_State* L)
{
    int n = lua_gettop(L);
    if(n != 1)
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Invalid number of arguments. (1 expected)");
        return 2;
    }
    std::string path = lua_tostring(L, -1);
    if(boost::filesystem::exists(path) && boost::filesystem::is_directory(path))
    {
        lua_pushboolean(L, true);
    }else
    {
        lua_pushboolean(L, false);
    }
    return 1;
}

int luacomputers::lua::filesystem::list(lua_State* L)
{
    int n = lua_gettop(L);
    if(n != 1)
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Invalid number of arguments. (1 expected)");
        return 2;
    }
    std::string path = lua_tostring(L, -1);
    if(boost::filesystem::exists(path) && boost::filesystem::is_directory(path))
    {
        size_t elements=0;
        std::vector<std::string> e;
        for(boost::filesystem::directory_entry& entry : boost::make_iterator_range(boost::filesystem::directory_iterator(path), {}))
        {
            std::string s = entry.path().filename().string();
            e.push_back(s);
            elements++;
        }
        lua_createtable(L, elements, 0);

	std::sort(e.begin(), e.end());

        //Vectors somehow work best when popping from behind ;)
        for(int i=elements; i>0; i--)
        {
            lua_pushinteger(L, i);
            lua_pushstring(L, e.back().c_str());
            lua_settable(L, -3);
            e.pop_back();
        }
        return 1;
    }else
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Path is not a directory / does not exist.");
        return 2;
    }
}

int luacomputers::lua::filesystem::size(lua_State* L)
{
    int n = lua_gettop(L);
    if(n != 1)
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Invalid number of arguments. (1 expected)");
        return 2;
    }
    std::string path = lua_tostring(L, -1);
    if(boost::filesystem::exists(path) && !boost::filesystem::is_directory(path))
    {
        lua_pushinteger(L, boost::filesystem::file_size(path));
        return 1;
    }else
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Path is a directory / does not exist.");
        return 2;
    }
}

int luacomputers::lua::filesystem::freeSpace(lua_State* L)
{
    int n = lua_gettop(L);
    if(n != 1)
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Invalid number of arguments. (1 expected)");
        return 2;
    }
    std::string path = lua_tostring(L, -1);
    if(boost::filesystem::exists(path) && boost::filesystem::is_directory(path))
    {
        lua_pushinteger(L, boost::filesystem::space(path).available);
        return 1;
    }else
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Path is not a directory / does not exist.");
        return 2;
    }
}

int luacomputers::lua::filesystem::move(lua_State* L)
{
    int n = lua_gettop(L);
    if(n != 2)
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Invalid number of arguments. (2 expected)");
        return 2;
    }
    std::string path1 = lua_tostring(L, 1);
    std::string path2 = lua_tostring(L, 2);
    if(boost::filesystem::exists(path1) && !boost::filesystem::exists(path2))
    {
        boost::filesystem::copy(path1, path2);
        if(boost::filesystem::is_directory(path1))
        {
            boost::filesystem::remove_all(path1);
        }else
        {
            boost::filesystem::remove(path1);
        }
        return 0;
    }else
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Path1 is does not exist or Path2 exists.");
        return 2;
    }
}

int luacomputers::lua::filesystem::copy(lua_State* L)
{
    int n = lua_gettop(L);
    if(n != 2)
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Invalid number of arguments. (2 expected)");
        return 2;
    }
    std::string path1 = lua_tostring(L, 1);
    std::string path2 = lua_tostring(L, 2);
    if(boost::filesystem::exists(path1) && !boost::filesystem::exists(path2))
    {
        boost::filesystem::copy(path1, path2);
        return 0;
    }else
    {
        lua_pushboolean(L, false);
        if(!boost::filesystem::exists(path1))
        {
            std::string s("Path1 is does not exist.");
            s.append(path1);
            lua_pushstring(L, s.c_str());
        }else if(boost::filesystem::exists(path2))
        {
            lua_pushstring(L, "Path2 exists.");
        }else 
        {
            lua_pushstring(L, "Dunno");
        }
        
        return 2;
    }
}

int luacomputers::lua::filesystem::remove(lua_State* L)
{
    int n = lua_gettop(L);
    if(n != 1)
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Invalid number of arguments. (1 expected)");
        return 2;
    }
    std::string path = lua_tostring(L, 1);
    if(boost::filesystem::exists(path))
    {
        if(boost::filesystem::is_directory(path))
        {
            boost::filesystem::remove_all(path);
        }else
        {
            boost::filesystem::remove(path);
        }
        return 0;
    }else
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Path1 is does not exist or Path2 exists.");
        return 2;
    }
}
