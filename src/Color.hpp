#pragma once
namespace luacomputers
{
    struct Color
    {
        uint16_t r;
        uint16_t g;
        uint16_t b;
    };
}