#include "Pixel.hpp"

using namespace luacomputers;

Pixel::Pixel() : colorIndex(15)
{
    rect = new SDL_Rect();
    rect->x = 0;
    rect->y = 0;
    rect->w = 1;
    rect->h = 1;
}

Pixel::Pixel(int x, int y, int w, int h, uint16_t colorIndex) : colorIndex(colorIndex)
{
    rect = new SDL_Rect();
    rect->x = x;
    rect->y = y;
    rect->w = w;
    rect->h = h;
}

Pixel::~Pixel()
{
    delete rect;
    rect = nullptr;
}

void Pixel::draw(SDL_Renderer* r, Color& color)
{
    SDL_SetRenderDrawColor(r, color.r, color.g, color.b, 0xFF);
    SDL_RenderFillRect(r, rect);
}

void Pixel::setWidth(int width)
{
    rect->w = width;
}

void Pixel::setHeight(int height)
{
    rect->h = height;
}

int Pixel::getWidth()
{
    int width;
    width = rect->w;
    return width;
}

int Pixel::getHeight()
{
    int height;
    height = rect->h;
    return height;
}

void Pixel::setColor(uint16_t colorIndex)
{
    this->colorIndex = colorIndex;
}