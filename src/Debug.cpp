#include "Debug.hpp"

void luacomputers::log(const char* name, const char* text)
{
    std::cout << "[" << name << "] " << text << std::endl;
}

void luacomputers::logError(const char* name, const char* text)
{
    std::cerr << "[" << name << "] " << text << std::endl;
}

void luacomputers::debug(const char* name, const char* text)
{
    if(luacomputers::isDebug)
    {
        std::cout << "[" << name << "] " << text << std::endl;
    }
}