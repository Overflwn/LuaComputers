#pragma once
#include <string>
#include <SFML/Network.hpp>
#include <mutex>
//TODO: find some good and cross-platform way to handle sockets

namespace luacomputers
{
	namespace networking
	{
		class UDPSock
		{
			private:

			bool init;
			sf::UdpSocket socket;
			std::string data;
			sf::Packet packet;
			sf::IpAddress sender;
			unsigned short senderPort;
			unsigned short port;

			std::mutex mut;

			public:
			// Random available port
			UDPSock();
			// Specific port, may throw errors.
			UDPSock(uint16_t port);
			~UDPSock();
			bool receive();
			bool isInit() {return init;}
			std::string getData() {return data;}
			bool sendMessage(std::string data, std::string dest, uint16_t port);
			unsigned short getPort();
		};
	}
}