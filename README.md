# This branch is an attempt to rewrite LuaComputers in SDL2

The reason for that is my spaghetti code that I wrote in the original version, making it very tedious to fix various issues.

I also misunderstood how the Lua binds work which I now understand a little bit more, making the code simpler in theory.

As I'm writing the code again from the ground up, it'll take more time because looking at the old code is not an option.
I basically have to rethink the whole infrastructure.

## Some new features I thought of

- Multiple computer instances
- <del>Text & Draw mode. (Text have a fixed size and text mode is basically like ComputerCraft drawing, you can manipulate every single pixel in draw mode though)</del>
  - **NEW:** Instead of a hard-coded text mode, implement double buffering / buffer "flushing" to enable faster drawing
  - The buffers allow to draw many pixels really fast, making drawing text and other things fast

## What I'm aiming to fix right from the beginning

- Weird segfaults caused by having unnecessary threads accessing things unsafely. (-> eliminate the use of unneeded threading and make shared resources thread-safe (mutex))

# Current TODO list
* [x] Change color system, create some sort of palette.
  * [x] Instead of giving the terminal / pixels the actual color, somehow give them just the index in the palette.
*	[x] The palette is editable, but this in turn updates the already drawn pixels.
*	[x] 256 colors instead of 16
*	[x] Color palettes are available per-terminal / computer
* [x] Add more term methods
  * [x] Scrolling (Vertical; maybe horizontal)
  * [x] Clearing / Filling rectangles (TODO: Test)
  * [x] Add a drawable "pixel buffer" for the term API -> allows to draw characters fast (well and it's basically just a double buffer)
  * [x] No hard-coded characters
* [x] Change Filesystem access
  * [x] Set the "root" to a folder specifically for the computer ID. (Override Lua io functions? Or add additional functions?)
* [ ] HTTP Requests
* [x] UDP Socket (shared by every computer?)
  * [x] Find a cross-platform, simple networking library
  * [x] Depending on the library, put sockets into seperate threads
* [ ] Encapsulate the Lua VM (disable debug library?)
* [ ] Configuration
  * [ ] Use an external config file to create a custom amount of computers with custom IDs, custom resolutions, etc.
* [ ] Audio (worst thing of all)
  * [ ] 8-bit audio frequencies?
  * [ ] Bind one .mp3 at a time as some sort of audiodisk? (-> CraftOS-PC2)
