#include <SDL2/SDL.h>
#include <SDL2/SDL_net.h>
#include <stdio.h>
#include <iostream>
#include "Pixel.hpp"
#include "Terminal.hpp"
#include "Computer.hpp"
#include <thread>
#include "UDPSock.hpp"


extern const double VERSION = 0.1;

/*
How this shit currently works:
- Create terminal object
- Create socket object
- Create computer object
  - computer initializes Lua VM
  - binds terminal, computer and socket object pointers to entries in the Lua Registry
  - binds C functions to lua_State
  - creates coroutine and initializes it (first run)
- Create ComputerThread objects for every computer
- Create std::thread for every ComputerThread object (start instantly)
- Draw loop starts
  - Gets SDL_Events
  - Passes these to the terminals and computer objects (the necessary methods are made thread-safe with std::mutex)
    - computer objects put the SDL_Events into a queue, which get processed later
    -> ensures that computers don't skip events by buffering them
    - terminal objects just check for window events
- Check if every computer is killed or every terminal is closed.

NOTE:
- Most of the methods in the Terminal class are made thread-safe via std::mutex
  - We need this as the main thread and the computer thread share the Terminal object
- The Lua C functions are static / not computer-bound.
  - They get which computer is currently calling it by receiving the computer / terminal / socket object pointer from the Lua Registry. (Idea from https://github.com/MCJack123/craftos2 , I'm really thankful for that)
*/

class ComputerThread
{
    private:
    luacomputers::Computer* c;

    public:
    ComputerThread(luacomputers::Computer* c) : c(c)
    {

    }
    void operator()() const {
        while(c->isRunning())
        {
            if(!c->run())
                std::cerr << "Computer somehow shut down or errored out." << std::endl;
        }
    }
};

int main(int argc, char **argv)
{
    printf("Welcome to LuaComputers v%g!\n", VERSION);
	bool testing_net = false;
    if(argc == 2)
    {
        if(argv[1][0] = '1')
            testing_net=true;
    }
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        std::cerr << "SDL_Init Error: " << SDL_GetError() << std::endl;
        return 1;
    }

    if(testing_net)
    {
        luacomputers::networking::UDPSock sock(6968);
        if(!sock.isInit())
        {
            std::cerr << "UDPSock error." << std::endl;
            return 1;
        }else
        {
            sock.sendMessage(std::string("hello"), std::string("127.0.0.1"), 6969);
            while(!sock.receive())
            {
                //std::cout << "waiting..." << std::endl;
            }
            std::cout << sock.getData().c_str() << std::endl;
			SDL_Quit();
            return 0;
        }
    }

    /*
    ComputerCraft Resolution:
    51x19
    -> Multiplied by 6 and 9 respectively: 306x171
    -> Multiplied by 4 and 4 respecitvely as window resolution: 1224x684 (=> one "pixel" is 4x4 actual pixels big)
    */
    luacomputers::Terminal* t = new luacomputers::Terminal(1224, 684, 306, 171, "Term1");
    luacomputers::networking::UDPSock* s = new luacomputers::networking::UDPSock(6968);
    if(!t->initialize() || !s->isInit())
    {
        delete t;
        delete s;
        SDL_Quit();
        return 1;
    }

    luacomputers::Computer* c = new luacomputers::Computer(0);
    if(!c->initialize(t, s))
    {
        delete t;
        delete c;
        SDL_Quit();
        return 1;
    }

    /*luacomputers::Terminal* t2 = new luacomputers::Terminal(1224, 684, 306, 171, "Term2");
    luacomputers::networking::UDPSock* s2 = new luacomputers::networking::UDPSock(6969);
    if(!t2->initialize())
    {
        delete t;
        delete c;
        delete t2;
        delete s2;
        SDL_Quit();
        return 1;
    }

    luacomputers::Computer* c2 = new luacomputers::Computer(1);
    if(!c2->initialize(t2, s2))
    {
        delete t;
        delete c;
        delete t2;
        delete c2;
        SDL_Quit();
        return 1;
    }*/

    //test colors
    for(int i=0; i<50; i++)
    {
        t->setPixel(5+i, 50, 1);
        t->setPixel(5+i, 51, 3);
        //t2->setPixel(5+i, 50, 1);
        //t2->setPixel(5+i, 51, 3);
    }


    bool quit = false;
    SDL_Event e;
    ComputerThread ct(c);
    //ComputerThread ct2(c2);
    std::thread ct_thread(ct);
    //std::thread ct_thread2(ct2);
    
    while(!quit)
    {
        //std::cout << "Handling all events." << std::endl;
        while(SDL_PollEvent(&e) != 0)
        {
            
            if(e.type == SDL_QUIT)
            {
                quit = true;
            }

            t->handleEvent(e);
            //t2->handleEvent(e);
            if(!c->isRunning())
            {
                std::cerr << "The computer 1 is somehow shut down." << std::endl;
            }else
            {
                c->putEvent(e);
            }
            /*if(!c2->isRunning())
            {
                std::cerr << "The computer 2 is somehow shut down." << std::endl;
            }else
            {
                c2->putEvent(e);
            }*/
        }
        //std::cout << "Handled all events. Checking Computer." << std::endl;
        //if(!c->isRunning() && !c2->isRunning())
        if(!c->isRunning())
        {
            std::cerr << "The computers are somehow shut down." << std::endl;
            quit = true;
        }
        //std::cout << "Checked. Drawing terminal." << std::endl;
        t->draw();
        //t2->draw();
        //std::cout << "Terminal drawn. Checking term status." << std::endl;

        
        //if(!t->isShown() && !t->isMinimized() && !t2->isShown() && !t2->isMinimized())
        if(!t->isShown() && !t->isMinimized())
            quit = true;
        //std::cout << "Term status checked." << std::endl;
    }

    ct_thread.join();
    //ct_thread2.join();

    delete t;
    std::cout << "Deleted t" << std::endl;
    delete c;
    std::cout << "Deleted c" << std::endl;
    /*delete t2;
    std::cout << "Deleted t2" << std::endl;
    delete c2;
    std::cout << "Deleted c2" << std::endl;*/
    SDL_Quit();

    return 0;
}