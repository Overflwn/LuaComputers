--[[
    Term API

    Makes use of buffers to create a text-based terminal.
]]

local fonts = {}
local l, err = fs.list("/os/fonts/")
if assert_type(l, "table") then
    for _, name in ipairs(l) do
        local file, err = io.open("/os/fonts/"..name, "r")
        if not file then
            kernel_log("term.lua", "Error reading "..name..": "..tostring(err))
        else
            local data = file:read("a")
            if data ~= nil and #data > 0 then
                local n = name
                local i, j = string.find(n, "%.")
                if i and i ~= -1 then
                    if i == 1 then
                        n = string.sub(n, 2)
                    else
                        n = string.sub(n, 1, i-1)
                    end
                end
                fonts[n] = json.decode(data)
            end
        end
    end
else
    kernel_log("term.lua", "Error listing /os/fonts/: "..tostring(err))
end

local font = fonts["ugly"]
if not assert_type(font, "table") then
    kernel_log("term.lua", "WARNING: The standard font (ugly) is not available.")
end

-- Every char should be the same dimension anyway
local char_width = #font["A"][1][1]
local char_height = #font["A"][1]
-- 1 is usually white
local font_col = 0
local back_col = 15
local w = math.floor(term.getWidth() / char_width)
local h = math.floor(term.getHeight() / char_height)



-- If we can't fill the whole screen with characters, we center the terminal
local offset_x = math.floor(math.fmod(term.getWidth(), char_width) / 2)
local offset_y = math.floor(math.fmod(term.getHeight(), char_height) / 2)

print(w.."x"..h.." "..offset_x.."|"..offset_y)

local pos_x = 1
local pos_y = 1

-- Term buffer
local chars = {}
for i=1, h do
    chars[i] = {}
    for j=1, w do
        chars[i][j] = " "
    end
end

print(type(chars[1][1]))

local t = {}

local function updateTerminal()
    local buffer = {}
    for i=1, term.getHeight() do
        buffer[i] = {}
        for j=1, term.getWidth() do
            buffer[i][j] = 256
        end
    end
    for i=1, h do
        for j=1, w do
            for y=1, #font[chars[i][j]][1] do
                for x=1, #font[chars[i][j]][1][y] do
                    local col = font_col
                    local sub = string.sub(font[chars[i][j]][1][y], x, x)
                    if sub == "x" then
                        col = font_col
                    else
                        col = back_col
                    end
                    buffer[(i-1)*char_height+offset_y+y][(j-1)*char_width+offset_x+x] = col
                end
            end
        end
    end
    term.flushBuffer(buffer, 0, 0, term.getWidth(), term.getHeight())
end

local function updatePixel(j, i)
    local buffer = {}
    for i=1, char_height do
        buffer[i] = {}
        for j=1, char_width do
            buffer[i][j] = 256
        end
    end
    for y=1, #font[chars[i][j]][1] do
        for x=1, #font[chars[i][j]][1][y] do
            local col = font_col
            local sub = string.sub(font[chars[i][j]][1][y], x, x)
            if sub == "x" then
                col = font_col
            else
                col = back_col
            end
            buffer[y][x] = col
        end
    end
    term.flushBuffer(buffer, (j-1)*char_width + offset_x, (i-1)*char_height + offset_y, char_width, char_height)
end

function t.clear()
    for y=1, h do
        for x=1, w do
            chars[y][x] = " "
        end
    end
    local buffer = {}
    for y=1, term.getHeight() do
        buffer[y] = {}
        for x=1, term.getWidth() do
            buffer[y][x] = back_col
        end
    end
    term.flushBuffer(buffer, 0, 0, term.getWidth(), term.getHeight())
end

function t.scroll(n)
    if n > 0 then
        if n < h then
            for j=1, n do
                for i=1, h-1 do
                    --Error: This copies the pointer (?)
                    --chars[i] = chars[i+1]
                    for x=1, w do
                        chars[i][x] = chars[i+1][x]
                    end
                end
            end
            for i=1, w do
                chars[h][i] = " "
            end
            updateTerminal()
        else
            t.clear()
        end
    elseif n < 0 then
        if math.abs(n) < h then
            for j=1, math.abs(n) do
                for i=h, 2, -1 do
                    chars[i] = chars[i-1]
                end
            end
            for i=1, w do
                chars[1][i] = " "
            end
            updateTerminal()
        else
            t.clear()
        end
    end
end

function t.write(text)
    if assert_type(text, "string") and #text > 0 then
        if #text > 1 then
            for i=1, #text do
                chars[pos_y][pos_x] = string.sub(text, i, i)
                if pos_x < w then
                    pos_x = pos_x+1
                elseif pos_y < h then
                    pos_x = 1
                    pos_y = pos_y+1
                else
                    t.scroll(1)
                    pos_y = h
                    pos_x = 1
                end
            end
            kernel_log("terminal", "Updating terminal.")
            updateTerminal()
        else
            chars[pos_y][pos_x] = string.sub(text, 1, 1)
            kernel_log("terminal", "Updating pixel ("..pos_x.."|"..pos_y..").")
            updatePixel(pos_x, pos_y)
            if pos_x < w then
                pos_x = pos_x+1
            elseif pos_y < h then
                pos_x = 1
                pos_y = pos_y+1
            else
                t.scroll(1)
                pos_y = h
                pos_x = 1
            end
        end
    end
end

return t