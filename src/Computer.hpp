/**
 * @file Computer.hpp
 * @author Overflwn (overflwn@gmail.com)
 * @brief The computer class which handles everything from creating the terminal, over initializing the LuaVM to setting up the network adapter.
 * @version 0.1
 * @date 2019-11-10
 * 
 * @copyright Copyright (c) 2019
 * 
 */


#pragma once
#include "Terminal.hpp"
#include "UDPSock.hpp"
#if __has_include(<lua.hpp>)
# include <lua.hpp>
#else
# include <lua5.3/lua.hpp>
#endif
#include <SDL2/SDL.h>
#include <vector>
#include <mutex>
#include <queue>

namespace luacomputers
{
    class Computer
    {
        private:
        unsigned int id;
        bool running;
        lua_State* L;
        lua_State* t;
        std::queue<SDL_Event> events;
        std::mutex m;

        public:
        Computer(unsigned int id);
        ~Computer();
        bool initialize(luacomputers::Terminal* term, luacomputers::networking::UDPSock* sock);
        bool run();
        void putEvent(SDL_Event e);
        bool isRunning();
        unsigned int getID();
    };
}