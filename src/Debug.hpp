#pragma once
#include <iostream>
namespace luacomputers
{
    static bool isDebug = false;

    void log(const char* name, const char* text);

    void logError(const char* name, const char* text);

    void debug(const char* name, const char* text);
}