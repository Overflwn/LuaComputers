/**
 * @file Lua_UDPSock.cpp
 * @author Overflwn (overflwn@gmail.com)
 * @brief (See Lua_UDPSock.hpp)
 * @version 0.1
 * @date 2019-11-20
 * 
 * @copyright Copyright (c) 2019
 * 
 */

#include "Lua_UDPSock.hpp"
#include "UDPSock.hpp"
#include <string>
#include "Debug.hpp"

luacomputers::networking::UDPSock* getSocket(lua_State* L)
{
    lua_pushstring(L, "socket");
    lua_gettable(L, LUA_REGISTRYINDEX);
    void* val = lua_touserdata(L, -1);
    lua_pop(L, 1);
    return (luacomputers::networking::UDPSock*)val;
}

int luacomputers::lua::UDPSock::receive(lua_State* L)
{
    int n = lua_gettop(L);
    if(n != 0)
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Invalid number of arguments. (0 expected)");
        return 2;
    }
    luacomputers::networking::UDPSock* sock = getSocket(L);
    if(sock == nullptr) 
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "LuaComputers error: Couldn't get socket pointer.");
        return 2;
    }else
    {
        lua_pushboolean(L, sock->receive());
        return 1;
    }
}

int luacomputers::lua::UDPSock::getMessage(lua_State* L)
{
    int n = lua_gettop(L);
    if(n != 0)
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Invalid number of arguments. (0 expected)");
        return 2;
    }
    luacomputers::networking::UDPSock* sock = getSocket(L);
    if(sock == nullptr) 
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "LuaComputers error: Couldn't get socket pointer.");
        return 2;
    }else
    {
        lua_pushstring(L, sock->getData().c_str());
        return 1;
    }
}

int luacomputers::lua::UDPSock::getPort(lua_State* L)
{
    int n = lua_gettop(L);
    if(n != 0)
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Invalid number of arguments. (0 expected)");
        return 2;
    }
    luacomputers::networking::UDPSock* sock = getSocket(L);
    if(sock == nullptr) 
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "LuaComputers error: Couldn't get socket pointer.");
        return 2;
    }else
    {
        std::string s("The port is ");
        s.append(std::to_string(sock->getPort()));
        luacomputers::log("Lua_UDPSock", s.c_str());
        lua_pushinteger(L, sock->getPort());
        return 1;
    }
}

int luacomputers::lua::UDPSock::sendMessage(lua_State* L)
{
    int n = lua_gettop(L);
    if(n != 3)
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "Invalid number of arguments. (3 expected)");
        return 2;
    }
    std::string msg(lua_tostring(L, 1));
    std::string ip(lua_tostring(L, 2));
    uint16_t port = lua_tointeger(L, 3);
    luacomputers::networking::UDPSock* sock = getSocket(L);
    if(sock == nullptr) 
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "LuaComputers error: Couldn't get socket pointer.");
        return 2;
    }else
    {
        bool result = sock->sendMessage(msg, ip, port);
        int n = 1;
        lua_pushboolean(L, result);
        if(!result)
        {
            n++;
            std::string s("Make sure the IP is in the correct format and the message size is below ");
            s.append(std::to_string(sf::UdpSocket::MaxDatagramSize));
            s.append(" bytes.");

            lua_pushstring(L, s.c_str());
        }
        
        return n;
    }
}