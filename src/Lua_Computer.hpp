#pragma once
#if __has_include(<lua.hpp>)
# include <lua.hpp>
#else
# include <lua5.3/lua.hpp>
#endif
namespace luacomputers
{
    namespace lua
    {
        namespace computer
        {
            int getID(lua_State* L);
        }
    }
}