/**
 * @file Computer.cpp
 * @author Overflwn (overflwn@gmail.com)
 * @brief Implementation of Computer.hpp
 * @version 0.1
 * @date 2019-11-10
 * 
 * @copyright Copyright (c) 2019
 * 
 */

#include "Computer.hpp"
#include "Lua_Terminal.hpp"
#include "Debug.hpp"
#include "Lua_UDPSock.hpp"
#include "Lua_FS.hpp"
#include "Lua_Computer.hpp"
using namespace luacomputers;

Computer::Computer(unsigned int id) : id(id), running(true)
{
}

Computer::~Computer()
{
    L = nullptr;
    t = nullptr;
}

static bool runVM(lua_State* t, int nargs);

/**
 * @brief Initialize the computer. (Bind C functions, etc.)
 * 
 * @return true Initialized successfully.
 * @return false Something happened.
 */
bool Computer::initialize(luacomputers::Terminal* term, luacomputers::networking::UDPSock* sock)
{
    luacomputers::log("Computer", "Initializing.");
    L = luaL_newstate();
    if(!L)
        return false;
    luaL_openlibs(L);
    

    t = lua_newthread(L);

    //other stuff
    int result = luaL_loadfile(t, "bios.lua");
    if(result != LUA_OK)
    {
        const char* msg = lua_tostring(t, -1);
        std::string s(msg);
        s = "Lua Error: " + s;
        luacomputers::logError("Computer", s.c_str());
        lua_pop(t, 1);
        return false;
    }

    

    //TODO: load custom libraries / commands
    lua_pushstring(t, "computer");
    lua_pushlightuserdata(t, this);
    lua_settable(t, LUA_REGISTRYINDEX);
    lua_pushstring(t, "terminal");
    lua_pushlightuserdata(t, term);
    lua_settable(t, LUA_REGISTRYINDEX);
    lua_pushstring(t, "socket");
    lua_pushlightuserdata(t, sock);
    lua_settable(t, LUA_REGISTRYINDEX);

    luaL_Reg term_api[] = {
        {
            "getPixel",
            luacomputers::lua::terminal::getPixel
        },
        {
            "setPixel",
            luacomputers::lua::terminal::setPixel
        },
        {
            "setPaletteColor",
            luacomputers::lua::terminal::setPaletteColor
        },
        {
            "getPaletteColor",
            luacomputers::lua::terminal::getPaletteColor
        },
        {
            "fillRect",
            luacomputers::lua::terminal::fillRect
        },
        {
            "clear",
            luacomputers::lua::terminal::clear
        },
        {
            "flushBuffer",
            luacomputers::lua::terminal::flushBuffer
        },
        {
            "getWidth",
            luacomputers::lua::terminal::getWidth
        },
        {
            "getHeight",
            luacomputers::lua::terminal::getHeight
        },
        {
            NULL,
            NULL
        }
    };
    luaL_newlib(t, term_api);

    luaL_Reg sock_api[] = {
        {
            "receive",
            luacomputers::lua::UDPSock::receive
        },
        {
            "getMessage",
            luacomputers::lua::UDPSock::getMessage
        },
        {
            "getPort",
            luacomputers::lua::UDPSock::getPort
        },
        {
            "sendMessage",
            luacomputers::lua::UDPSock::sendMessage
        },
        {
            NULL,
            NULL
        }
    };
    luaL_newlib(t, sock_api);

    luaL_Reg fs_api[] = {
        {
            "makeDir",
            luacomputers::lua::filesystem::makeDir
        },
        {
            "exists",
            luacomputers::lua::filesystem::exists
        },
        {
            "isDirectory",
            luacomputers::lua::filesystem::isDirectory
        },
        {
            "list",
            luacomputers::lua::filesystem::list
        },
        {
            "size",
            luacomputers::lua::filesystem::size
        },
        {
            "freeSpace",
            luacomputers::lua::filesystem::freeSpace
        },
        {
            "move",
            luacomputers::lua::filesystem::move
        },
        {
            "copy",
            luacomputers::lua::filesystem::copy
        },
        {
            "remove",
            luacomputers::lua::filesystem::remove
        },
        {
            NULL,
            NULL
        }
    };
    luaL_newlib(t, fs_api);

    luaL_Reg computer_api[] = {
        {
            "getID",
            luacomputers::lua::computer::getID
        },
        {
            NULL,
            NULL
        }
    };
    luaL_newlib(t, computer_api);

    //nargs = number of added libraries
    if(!runVM(t, 4))
        return false;
    luacomputers::log("Computer", "Initialized.");
    return true;
}

bool runVM(lua_State* t, int nargs)
{
    int result = lua_resume(t, NULL, nargs);
    //luacomputers::log("Computer", "Ran");
    switch(result)
    {
        case LUA_YIELD:
        //luacomputers::log("Computer", "Yielded.");
        return true;
        break;
        case LUA_OK:
        luacomputers::log("Computer", "Finished.");
        return false;
        break;
        case LUA_ERRRUN:
        {
            const char* msg = lua_tostring(t, -1);
            std::string s(msg);
            s = "Runtime Error: " + s;
            luacomputers::logError("Computer", s.c_str());
            lua_pop(t, 1);
            return false;
        }
        case LUA_ERRMEM:
        {
            const char* msg = lua_tostring(t, -1);
            std::string s(msg);
            s = "Memory allocation Error: " + s;
            luacomputers::logError("Computer", s.c_str());
            lua_pop(t, 1);
            return false;
        }
        case LUA_ERRERR:
        {
            const char* msg = lua_tostring(t, -1);
            std::string s(msg);
            s = "Message handler error: " + s;
            luacomputers::logError("Computer", s.c_str());
            lua_pop(t, 1);
            return false;
        }
        case LUA_ERRGCMM:
        {
            const char* msg = lua_tostring(t, -1);
            std::string s(msg);
            s = "__gcm metamethod error: " + s;
            luacomputers::logError("Computer", s.c_str());
            lua_pop(t, 1);
            return false;
        }
        default:
        return false;
    }
}

void Computer::putEvent(SDL_Event e)
{
    m.lock();
    events.push(e);
    m.unlock();
}

/**
 * @brief Resume the Lua coroutine. (With optional events like mouse or key inputs)
 * 
 * @param e The SDL_Event
 * @return true Coroutine yielded and no errors.
 * @return false Coroutine either finished or errored out.
 */
bool Computer::run()
{
    
    int nargs = 0;
    //std::cout << "Running Computer" << std::endl;
    lua_pushstring(t, "terminal");
    lua_gettable(t, LUA_REGISTRYINDEX);
    Terminal* val = (Terminal*)lua_touserdata(t, -1);
    lua_pop(t, 1);
    if(val == NULL)
    {
        luacomputers::logError("Computer", "Terminal pointer is somehow null");
        running = false;
        return false;
    }

    if(val->isClosed())
    {
        luacomputers::logError("Computer", "Terminal is completely closed.");
        running = false;
        return false;
    }

    int btn = 0;
    if(val->isFocused() && val->isShown() && !events.empty()) //It's possible that every window is minimized, which is why the last minimized window will be "focused" by SDL.
    {
        SDL_Event e = events.front();
        //Only if the window is focused and shown we handle keyboard and mouse events.
        switch(e.type)
        {
            case SDL_KEYDOWN:
            //https://wiki.libsdl.org/SDLScancodeLookup
            lua_pushstring(t, "KEY_DOWN");
            lua_pushinteger(t, e.key.keysym.scancode);
            nargs = 2;
            break;

            case SDL_TEXTINPUT:
            //https://wiki.libsdl.org/SDL_TextInputEvent
            lua_pushstring(t, "TEXT");
            lua_pushstring(t, e.text.text);
            nargs = 2;
            break;

            case SDL_KEYUP:
            //https://wiki.libsdl.org/SDLScancodeLookup
            lua_pushstring(t, "KEY_UP");
            lua_pushinteger(t, e.key.keysym.scancode);
            nargs = 2;
            break;

            case SDL_MOUSEMOTION:
            //https://wiki.libsdl.org/SDL_MouseMotionEvent
            lua_pushstring(t, "MOUSE_MOVE");
            lua_pushinteger(t, val->toTermX(e.motion.x));
            lua_pushinteger(t, val->toTermY(e.motion.y));
            nargs = 3;
            break;

            case SDL_MOUSEBUTTONDOWN:
            //https://wiki.libsdl.org/SDL_MouseButtonEvent
            lua_pushstring(t, "MOUSE_DOWN");
            btn = 0;
            switch(e.button.button) 
            {
                case SDL_BUTTON_LEFT:
                btn = 0;
                break;
                case SDL_BUTTON_MIDDLE:
                btn = 2;
                break;
                case SDL_BUTTON_RIGHT:
                btn = 1;
            }
            lua_pushinteger(t, btn);
            lua_pushinteger(t, val->toTermX(e.button.x));
            lua_pushinteger(t, val->toTermY(e.button.y));
            nargs = 4;
            break;

            case SDL_MOUSEBUTTONUP:
            //https://wiki.libsdl.org/SDL_MouseButtonEvent
            lua_pushstring(t, "MOUSE_UP");
            btn = 0;
            switch(e.button.button) 
            {
                case SDL_BUTTON_LEFT:
                btn = 0;
                break;
                case SDL_BUTTON_MIDDLE:
                btn = 2;
                break;
                case SDL_BUTTON_RIGHT:
                btn = 1;
            }
            lua_pushinteger(t, btn);
            lua_pushinteger(t, val->toTermX(e.button.x));
            lua_pushinteger(t, val->toTermY(e.button.y));
            nargs = 4;
            break;

            case SDL_MOUSEWHEEL:
            //https://wiki.libsdl.org/SDL_MouseWheelEvent
            lua_pushstring(t, "MOUSE_WHEEL");
            if(e.wheel.y < 0) 
            {
                lua_pushstring(t, "up");
            }else if(e.wheel.y > 0)
            {
                lua_pushstring(t, "down");
            }
            lua_pushinteger(t, e.wheel.x);
            lua_pushinteger(t, e.wheel.y);
            nargs = 4;
            break;
        }
        events.pop();
    }else if(!events.empty())
    {
        SDL_Event e = events.front();
        //TODO: Handle other events like networking
        events.pop();
    }  
    
    if(!runVM(t, nargs))
        running=false;
    return true;
}

bool Computer::isRunning()
{
    m.lock();
    bool r = running;
    m.unlock();
    return r;
}

unsigned int Computer::getID()
{
    unsigned int i;
    m.lock();
    i = id;
    m.unlock();
    return i;
}