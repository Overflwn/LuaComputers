#pragma once
#include <SDL2/SDL.h>
#include "Color.hpp"
#include <mutex>

namespace luacomputers {
    class Pixel {
        private:
        SDL_Rect* rect;
        uint16_t colorIndex;
        public:
        Pixel();
        Pixel(int x, int y, int w, int h, uint16_t colorIndex);
        ~Pixel();
        void draw(SDL_Renderer* r, Color& color);
        void setWidth(int width);
        void setHeight(int height);
        int getWidth();
        int getHeight();
        void setX(int x) {rect->x = x;}
        void setY(int y) {rect->y = y;}
        int getX() {return rect->x;}
        int getY() {return rect->y;}
        void setColor(uint16_t colorIndex);
        uint16_t getColor() {return colorIndex;}
    };
}