-- Example bios.lua

-- LuaComputers runs the script one time when initializing to pass you
-- the custom API tables, so I think you'd want to yield after receiving them.
local apis = {...}
_G.fs = {}
_G.term = {}
_G.computer = {}
_G.socket = {}

for name, func in pairs(apis[1]) do
    term[name] = func
end
for name, func in pairs(apis[2]) do
    socket[name] = func
end
for name, func in pairs(apis[3]) do
    fs[name] = func
end
for name, func in pairs(apis[4]) do
    computer[name] = func
end
coroutine.yield() -- Yield after initializing 
-- rest gets ran after the actual "start" of the computer
print("Hello LuaComputers! ID: "..computer.getID())
fs.makeDir("Computer/"..computer.getID())

-- Restrict filesystem to computer-specific folders.
local oldOpen = io.open
local oldMakeDir = fs.makeDir
local oldExists = fs.exists
local oldIsDirectory = fs.isDirectory
local oldLoadFile = loadfile
local oldList = fs.list
local oldSize = fs.size
local oldFreeSpace = fs.freeSpace
local oldMove = fs.move
local oldCopy = fs.copy
local oldRemove = fs.remove
-- Ignore escape attempts
local function fixPath(path)
    local p = path
    local i, j = string.find(p, "%.%./")
    while i ~= nil do
        local before = string.sub(p, 1, i-1)
        local after = string.sub(p, j+1)
        p = before..after
        i, j = string.find(p, "%.%./")
    end
    if string.sub(p, -3, -1) == "/%.%." then
        p = string.sub(p, 1, -4)
    end
    return p
end
function io.open(path, mode)
    local p = "Computer/"..computer.getID().."/"..fixPath(path)
    print("Opening "..p.." with mode "..mode)
    return oldOpen(p, mode)
end
function fs.makeDir(path)
    return oldMakeDir("Computer/"..computer.getID().."/"..fixPath(path))
end
function fs.exists(path)
    return oldExists("Computer/"..computer.getID().."/"..fixPath(path))
end
function fs.isDirectory(path)
    return oldIsDirectory("Computer/"..computer.getID().."/"..fixPath(path))
end
function fs.list(path)
    return oldList("Computer/"..computer.getID().."/"..fixPath(path))
end
function fs.size(path)
    return oldSize("Computer/"..computer.getID().."/"..fixPath(path))
end
function fs.freeSpace(path)
    return oldFreeSpace("Computer/"..computer.getID().."/"..fixPath(path))
end
function fs.move(path, path2)
    return oldMove(("Computer/"..computer.getID().."/"..fixPath(path)), ("Computer/"..computer.getID().."/"..fixPath(path2)))
end
function fs.copy(path, path2)
    return oldCopy(("Computer/"..computer.getID().."/"..fixPath(path)), ("Computer/"..computer.getID().."/"..fixPath(path2)))
end
function fs.remove(path)
    return oldRemove(("Computer/"..computer.getID().."/"..fixPath(path)))
end
function _G.loadfile(path, mode, env)
    local p = path
    print("Custom loadfile")
    if p ~= nil then
        p = "Computer/"..computer.getID().."/"..fixPath(path)
    end
    return oldLoadFile(p, mode, env)
end

function _G.assert_type(var, typ)
    if var and type(var) == typ then
        return true
    else
        return false
    end
end

function _G.kernel_log(name, message)
    if assert_type(name, "string") and assert_type(message, "string") then
        print("["..name.."] "..message)
    end
end

kernel_log("bios.lua", "Loading libraries...")
local libs, err = fs.list("/os/libs")
if libs and type(libs) == "table" then
    print("Libraries: "..tostring(#libs))
    for _, lib in ipairs(libs) do
        kernel_log("bios.lua", "Loading "..lib.."...")
        local f, err = loadfile("/os/libs/"..lib, "bt", _G)
        if f ~= nil then
            -- A library has to return a table containing functions
            local ok, ret = pcall(f)
            if ok then
                local name = lib
                local i, j = string.find(name, "%.")
                if i and i~=-1 then
                    if i == 1 then
                        name = string.sub(name, 2)
                    else
                        name = string.sub(name, 1, i-1)
                    end
                end
                i, j = string.find(name, "%d_%a")
                if i and i ~= -1 then
                    name = string.sub(name, j)
                end
                _G[name] = ret
                kernel_log("bios.lua", "Successfully loaded "..name)
            else
                kernel_log("bios.lua", "LIBRARY "..lib.." ERROR: "..tostring(ret))
            end
        else
            kernel_log("bios.lua", "Error loading "..lib..": "..tostring(err))
        end
    end

    
else
    print("Error: "..tostring(err))
    return
end

print(fixPath("//..//../hello/../test/a/b/.."))
local f, err = io.open("test.txt", "w")
if not f then
    print("Some error: "..tostring(err))
else
    f:write("Hello world")
    f:close()
end

local t, err = fs.list("/")
if t and type(t) == "table" then
    for a, b in ipairs(t) do
        print(b)
    end
else
    print("Couldn't list "..tostring(err))
end

print("Free space: " .. tostring(fs.freeSpace("/")))
print("File size: " .. tostring(fs.size("/test.txt")))
fs.move("test.txt", "text2.txt")
local succ, err = fs.copy("text2.txt", "text3.txt")
if not succ then
    print(tostring(err))
end
fs.move("text2.txt", "text4.txt")
if not fs.exists("text4.txt") then
    print("WARNING! File does not exist.")
else
    print("File exists.")
end

local red = false
if term ~= nil then
    term.setPixel(2, 2, 3)
    local data = {}
    for i=1, 10 do
        data[i] = {}
        for j=1, 10 do
            data[i][j] = 4
        end
    end
    term.flushBuffer(data, 4, 16, 10, 10);
    if _G["terminal"] ~= nil then
        terminal.write("HELLO WORLD")
    end
    while true do
	    local event, btn, x, y = coroutine.yield()
        local gotMsg = socket.receive()
	    if event == "MOUSE_DOWN" then
		    term.setPixel(x, y, 3)
	    -- let's switch up the polor palette ;)
	    elseif event == "TEXT" and btn == "s" then
		    if not red then
                term.setPaletteColor(1, 255, 0, 0)
                red = true
            else
                term.setPaletteColor(1, 0, 255, 0)
                red = false
            end
        elseif event == "TEXT" and btn == "d" then
            print("------------------------------------------------")
            local r, g, b = term.getPaletteColor(1)
            print("R: "..r.." G: "..g.." B: "..b)
        elseif event == "TEXT" and btn == "c" then
            term.clear(1)
        elseif event == "TEXT" and btn == "p" then
            local port, msg = socket.getPort()
            if not port then
                print("Error: "..tostring(msg))
            else
                print("Port: "..tostring(port))
            end
        elseif event == "TEXT" and btn == "m" then
            local port, msg = socket.getPort()
            if not port then
                print("Error: "..tostring(msg))
            else
                if port == 6969 then port = 6968 else port = 6969 end
                socket.sendMessage("hello", "127.0.0.1", port)
            end
        elseif event == "TEXT" and btn == "n" then
            local data2 = {}
            for i=1, 10 do
                data2[i] = {}
                for j=1, 10 do
                    data2[i][j] = 5
                end
            end
            data2[3][3] = 256;
            print("Okay, I'm trying to flush")
            term.flushBuffer(data2, 5, 5, 10, 10);
        elseif event == "TEXT" and btn == "b" then
            local succ, err = fs.copy("test2.txt", "test3.txt")
            if not succ then
                print(tostring(err))
            end
        end
        
        if event == "TEXT" then
            terminal.write(string.upper(btn))
        end

        if gotMsg then
            local msg = socket.getMessage()
            print("Message: "..msg)
        end
    end
else
    error("Terminal API not found.")
end

