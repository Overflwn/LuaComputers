/**
 * @file Lua_Terminal.hpp
 * @author Overflwn (overflwn@gmail.com)
 * @brief The C functions meant to be bound by the Lua states. (independable of computers)
 * @version 0.1
 * @date 2019-11-10
 * 
 * @copyright Copyright (c) 2019
 * 
 */

#pragma once
#if __has_include(<lua.hpp>)
# include <lua.hpp>
#else
# include <lua5.3/lua.hpp>
#endif

namespace luacomputers 
{
    namespace lua 
    {
        namespace terminal 
        {
            int setPixel(lua_State* L);
            int getPixel(lua_State* L);
            int setPaletteColor(lua_State* L);
            int getPaletteColor(lua_State* L);
            int fillRect(lua_State* L);
            int clear(lua_State* L);
            int flushBuffer(lua_State* L);
            int getWidth(lua_State* L);
            int getHeight(lua_State* L);
        }
    }
}