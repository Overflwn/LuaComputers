/**
 * @file Lua_UDPSock.hpp
 * @author Overflwn (overflwn@gmail.com)
 * @brief The C functions meant to be bound by the Lua states. (independable of computers)
 * @version 0.1
 * @date 2019-11-20
 * 
 * @copyright Copyright (c) 2019
 * 
 */

#pragma once
#if __has_include(<lua.hpp>)
# include <lua.hpp>
#else
# include <lua5.3/lua.hpp>
#endif

namespace luacomputers 
{
    namespace lua 
    {
        namespace UDPSock 
        {
            int receive(lua_State* L);
            int getMessage(lua_State* L);
            int getPort(lua_State* L);
            int sendMessage(lua_State* L);
        }
    }
}