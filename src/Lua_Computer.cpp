#include "Lua_Computer.hpp"
#include "Computer.hpp"

luacomputers::Computer* getComputer(lua_State* L)
{
    lua_pushstring(L, "computer");
    lua_gettable(L, LUA_REGISTRYINDEX);
    void* val = lua_touserdata(L, -1);
    lua_pop(L, 1);
    return (luacomputers::Computer*)val;
}

int luacomputers::lua::computer::getID(lua_State* L)
{
    luacomputers::Computer* comp = getComputer(L);
    if(comp == nullptr)
    {
        lua_pushboolean(L, false);
        lua_pushstring(L, "LuaComputers error: Couldn't get computer pointer.");
        return 2;
    }else
    {
        unsigned int id = comp->getID();

        lua_pushinteger(L, id);
        return 1;
    }
}