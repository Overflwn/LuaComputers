#pragma once

#if __has_include(<lua.hpp>)
# include <lua.hpp>
#else
# include <lua5.3/lua.hpp>
#endif

namespace luacomputers 
{
    namespace lua 
    {
        namespace filesystem 
        {
            int makeDir(lua_State* L);
            int exists(lua_State* L);
            int isDirectory(lua_State* L);
            int list(lua_State* L);
            int size(lua_State* L);
            int freeSpace(lua_State* L);
            int move(lua_State* L);
            int copy(lua_State* L);
            int remove(lua_State* L);
        }
    }
}