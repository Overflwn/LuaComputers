/**
 * @file Terminal.hpp
 * @author Overflwn (overflwn@gmail.com)
 * @brief The terminal class which handles SDL calls and draws the pixels on the screen.
 * @version 0.5
 * @date 2019-11-05
 * 
 * @copyright Copyright (c) 2019
 * 
 */

#pragma once

#include <iostream>
#include <mutex>
#include <SDL2/SDL.h>
#include <string>
#include "Pixel.hpp"

namespace luacomputers {
    class Terminal {
        private:
        SDL_Window* sdlWindow;
        SDL_Renderer* sdlRend;
        int width;
        int height;
        int pix_x;
        int pix_y;
        int pix_w;
        int pix_h;
        std::string title;
        luacomputers::Pixel*** pixels;
        Uint32 fps_lasttime;
        Uint32 fps_current;
        Uint32 fps_frames;
        int windowID;
        bool shown;
        bool minimized;
        bool focused;
        bool closed;
        void updatePixels();

        Color colors[256];

        std::mutex mutex;

        public:
        Terminal();
        Terminal(int width, int height, int term_width, int term_height, const char* title);
        ~Terminal();
        bool initialize();
        void draw();
        void handleEvent(SDL_Event& e);
        void focus();
        int getWindowID() {return windowID;}
        bool isFocused() {return focused;}
        bool isShown() {return shown;}
        bool isMinimized() {return minimized;}
        void setPixel(unsigned int x, unsigned int y, uint16_t i);
        uint16_t getPixel(unsigned int x, unsigned int y);

        void fillRect(unsigned int x, unsigned int y, unsigned int width, unsigned int height, uint16_t col);
        void clear(uint16_t col);
        void flushBuffer(unsigned int x, unsigned int y, unsigned int width, unsigned int height, uint16_t** data);

        void setPaletteColor(uint16_t index, Color col);
        Color getPaletteColor(uint16_t index);
        int getTermWidth() {return pix_x;}
        int getTermHeight() {return pix_y;}
        int toTermX(int windowX);
        int toTermY(int windowY);
        bool isClosed() {return closed;}
    };
}