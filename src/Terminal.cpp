/**
 * @file Terminal.cpp
 * @author Overflwn (overflwn@gmail.com)
 * @brief The implementation of Terminal.hpp
 * @version 0.5
 * @date 2019-11-05
 * 
 * @copyright Copyright (c) 2019
 * 
 */

#include "Terminal.hpp"
#include "Color.hpp"
#include <math.h>
#include "Debug.hpp"

using namespace luacomputers;

/**
 * @brief Construct a new Terminal with default values. (800, 600, 100, 100, "LuaComputers Terminal")
 * 
 */
Terminal::Terminal() : width(800),
                       height(600),
                       pix_x(100),
                       pix_y(100),
                       pix_w(width/pix_x),
                       pix_h(height/pix_y),
                       sdlWindow(nullptr),
                       sdlRend(nullptr),
                       title("LuaComputers Terminal"),
                       fps_lasttime(SDL_GetTicks()),
                       fps_frames(0),
                       shown(true),
                       focused(false),
                       minimized(false),
                       closed(false)
{
    //TODO: initialize other stuff

    //NOTE: Colors taken from ComputerCraft
    colors[0] = {0xF0,0xF0,0xF0};
    colors[1] = {0xF2,0xB2,0x33};
    colors[2] = {0xE5,0x7f,0xd8};
    colors[3] = {0x99,0xb2,0xf2};
    colors[4] = {0xde,0xde,0x6c};
    colors[5] = {0x7f,0xcc,0x19};
    colors[6] = {0xf2,0xb2,0xcc};
    colors[7] = {0x4c,0x4c,0x4c};
    colors[8] = {0x99,0x99,0x99};
    colors[9] = {0x4c,0x99,0xb2};
    colors[10] = {0xb2,0x66,0xe5};
    colors[11] = {0x33,0x66,0xcc};
    colors[12] = {0x7f,0x66,0x4c};
    colors[13] = {0x57,0xa6,0x4e};
    colors[14] = {0xcc,0x4c,0x4c};
    colors[15] = {0x19,0x19,0x19};
    pixels = new luacomputers::Pixel**[pix_y];
    for(int y=0; y<pix_y; y++)
    {
        pixels[y] = new luacomputers::Pixel*[pix_x];
        for(int x=0; x<pix_x; x++)
        {
            pixels[y][x] = new luacomputers::Pixel(x*pix_w, y*pix_h, pix_w, pix_h, 15);
        }
    }
}

/**
 * @brief Construct a new Terminal for a computer.
 * 
 * @param width The window width.
 * @param height The window size.
 * @param term_width The amount of pixels horizontally.
 * @param term_height The amount of pixels vertically.
 * @param title The window title.
 */
Terminal::Terminal(int width, int height, int term_width, int term_height, const char *title) : width(width),
                                                                                                height(height),
                                                                                                pix_x(term_width),
                                                                                                pix_y(term_height),
                                                                                                pix_w(width/pix_x),
                                                                                                pix_h(height/pix_y),
                                                                                                sdlWindow(nullptr),
                                                                                                sdlRend(nullptr),
                                                                                                title(title),
                                                                                                fps_lasttime(SDL_GetTicks()),
                                                                                                fps_frames(0),
                                                                                                shown(true),
                                                                                                focused(false),
                                                                                                minimized(false),
                                                                                                closed(false)
{
    //NOTE: Colors taken from ComputerCraft
    colors[0] = {0xF0,0xF0,0xF0};
    colors[1] = {0xF2,0xB2,0x33};
    colors[2] = {0xE5,0x7f,0xd8};
    colors[3] = {0x99,0xb2,0xf2};
    colors[4] = {0xde,0xde,0x6c};
    colors[5] = {0x7f,0xcc,0x19};
    colors[6] = {0xf2,0xb2,0xcc};
    colors[7] = {0x4c,0x4c,0x4c};
    colors[8] = {0x99,0x99,0x99};
    colors[9] = {0x4c,0x99,0xb2};
    colors[10] = {0xb2,0x66,0xe5};
    colors[11] = {0x33,0x66,0xcc};
    colors[12] = {0x7f,0x66,0x4c};
    colors[13] = {0x57,0xa6,0x4e};
    colors[14] = {0xcc,0x4c,0x4c};
    colors[15] = {0x19,0x19,0x19};
    
    pixels = new luacomputers::Pixel**[pix_y];
    for(int y=0; y<pix_y; y++)
    {
        pixels[y] = new luacomputers::Pixel*[pix_x];
        for(int x=0; x<pix_x; x++)
        {
            pixels[y][x] = new luacomputers::Pixel(x*pix_w, y*pix_h, pix_w, pix_h, 15);
        }
    }
}

/**
 * @brief Destroy SDL objects and other pointers. NOTE: This does NOT quit SDL. That has to be done by the main thread.
 * 
 */
Terminal::~Terminal()
{
    for (int y=0; y<pix_y; y++)
    {
        delete[] pixels[y];
        pixels[y] = nullptr;
    }
    delete[] pixels;
    pixels = nullptr;
    SDL_DestroyRenderer(sdlRend);
    SDL_DestroyWindow(sdlWindow);
}

/**
 * @brief Initializes the SDL window and renderer.
 * 
 * @return true if everything went fine.
 * @return false if SDL threw an error and prints it out into std::cerr.
 */
bool Terminal::initialize() 
{
    sdlWindow = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_SHOWN);
    if (sdlWindow == nullptr)
    {
        std::cerr << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
        return false;
    }

    sdlRend = SDL_CreateRenderer(sdlWindow, -1, SDL_RENDERER_ACCELERATED);
    if (sdlRend == nullptr)
    {
        SDL_DestroyWindow(sdlWindow);
        std::cerr << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
        return false;
    }
    windowID = SDL_GetWindowID(sdlWindow);
    return true;
}

/**
 * @brief Draws the Terminal window
 * 
 */
void Terminal::draw()
{
    mutex.lock();
    if(shown)
    {
        SDL_SetRenderDrawColor(sdlRend, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderClear(sdlRend);

        for(int y=0; y<pix_y; y++)
        {
            for(int x=0; x<pix_x;x++)
            {
                //Random color
                //pixels[y][x]->setColor(rand()%256, rand()%256, rand()%256);
                pixels[y][x]->draw(sdlRend, colors[pixels[y][x]->getColor()]);
            }
        }
        //luacomputers::debug("Terminal", "Drawn pixels");

        fps_frames++;
        if(fps_lasttime < SDL_GetTicks() - 1*1000)
        {
            fps_lasttime = SDL_GetTicks();
            fps_current = fps_frames;
            fps_frames = 0;
        }
        std::cout << "Window ID: " << windowID << " FPS: " << fps_current << std::endl;

        SDL_RenderPresent(sdlRend);
    }
    mutex.unlock();
}

/**
 * @brief Update the pixel size incase the window size changed
 * 
 */
void Terminal::updatePixels()
{
    mutex.lock();
    for(int y=0; y<pix_y; y++) 
    {
        for(int x=0; x<pix_x; x++)
        {
            pixels[y][x]->setX(x*pix_w);
            pixels[y][x]->setY(y*pix_h);
            pixels[y][x]->setWidth(pix_w);
            pixels[y][x]->setHeight(pix_h);
        }
    }
    mutex.unlock();
}

/**
 * @brief Handle an event that occured inside that window.
 * 
 * @param e The event. 
 */
void Terminal::handleEvent(SDL_Event& e)
{
    mutex.lock();
    if(e.type == SDL_WINDOWEVENT && e.window.windowID == windowID)
    {
        std::string s;
        switch(e.window.event)
        {
            case SDL_WINDOWEVENT_SHOWN:
            minimized=false;
            shown = true;
            break;
            case SDL_WINDOWEVENT_HIDDEN:
            shown = false;
            break;
            case SDL_WINDOWEVENT_SIZE_CHANGED:
            width = e.window.data1;
            height = e.window.data2;
            pix_w = width/pix_x;
            pix_h = height/pix_y;
            updatePixels();
            draw();
            break;
            case SDL_WINDOWEVENT_EXPOSED:
            SDL_RenderPresent(sdlRend);
            break;
            case SDL_WINDOWEVENT_FOCUS_GAINED:
            s = "Focused gained: " + title;
            luacomputers::debug("Terminal", s.c_str());
            focused = true;
            break;
            case SDL_WINDOWEVENT_FOCUS_LOST:
            s = "Focus lost: " + title;
            luacomputers::debug("Terminal", s.c_str());
            focused = false;
            break;
            case SDL_WINDOWEVENT_MINIMIZED:
            minimized=true;
            shown = false;
            break;
            case SDL_WINDOWEVENT_MAXIMIZED:
            minimized=false;
            shown = true;
            break;
            case SDL_WINDOWEVENT_RESTORED:
            minimized = false;
            shown = true;
            break;
            case SDL_WINDOWEVENT_CLOSE:
            SDL_HideWindow(sdlWindow);
            closed=true;
            break;
        }
    }
    mutex.unlock();
}

/**
 * @brief Show the window and focus it.
 * 
 */
void Terminal::focus()
{
    mutex.lock();
    if(!shown)
    {
        SDL_ShowWindow(sdlWindow);
    }
    SDL_RaiseWindow(sdlWindow);
    mutex.unlock();
}

/**
 * @brief Set the specified pixel to the specified color.
 * 
 * @param x 
 * @param y 
 * @param i Index of the color in the cols array.
 */
void Terminal::setPixel(unsigned int x, unsigned int y, uint16_t i)
{
    mutex.lock();
    if(x < pix_x && y < pix_y)
    {
        if(i<256)
        {
            pixels[y][x]->setColor(i);
        }
        else
        {
            pixels[y][x]->setColor(255);
        }
    }
    mutex.unlock();
}

/**
 * @brief Get the color of the specified pixel.
 * 
 * @param x 
 * @param y 
 * @return uint16_t The index of the color in the colors array.
 */
uint16_t Terminal::getPixel(unsigned int x, unsigned int y)
{
    mutex.lock();
    uint16_t i = 15;
    if(x < pix_x && y < pix_y)
    {
        i = pixels[y][x]->getColor();
    }
    mutex.unlock();
    return i;
}

/**
 * @brief Converts the window x coordinate to a terminal y coordinate.
 * 
 * @param windowX 
 * @return int 
 */
int Terminal::toTermX(int windowX)
{
    double result;
    mutex.lock();
    result = std::floor(windowX/pix_w);
    mutex.unlock();
    return result;
}

/**
 * @brief Converts the window y coordinate to a terminal y coordinate.
 * 
 * @param windowY 
 * @return int 
 */
int Terminal::toTermY(int windowY)
{
    double result;
    mutex.lock();
    result = std::floor(windowY/pix_h);
    mutex.unlock();
    return result;
}

void Terminal::setPaletteColor(uint16_t index, Color col)
{
    mutex.lock();
    if(index < 256)
    {
        colors[index] = col;
    }
    mutex.unlock();
}

Color Terminal::getPaletteColor(uint16_t index)
{
    luacomputers::Color col;
    mutex.lock();
    if(index < 256)
    {
        col = colors[index];
    }else
    {
        col = colors[255];
    }
    mutex.unlock();
    return col;
}

void Terminal::fillRect(unsigned int x, unsigned int y, unsigned int width, unsigned int height, uint16_t col)
{
    mutex.lock();
    for(int i=y; i<y+height; i++)
    {
        for(int j=x; j<x+width; j++)
        {
            if(j < pix_x && i < pix_y)
            {
                if(col<256)
                {
                    pixels[i][j]->setColor(col);
                }
                else
                {
                    pixels[i][j]->setColor(255);
                }
            }
        }
    }
    mutex.unlock();
}
void Terminal::clear(uint16_t col)
{
    fillRect(0, 0, this->pix_x, this->pix_y, col);
}

/**
 * @brief Flush a two-dimensional array onto the pixels array.
 *        NOTE: Colors 256+ are considered transparent; coordinates out of bounds
 *              are ignored.
 * 
 * @param x
 * @param y
 * @param width 
 * @param height 
 * @param data 
 */
void Terminal::flushBuffer(unsigned int x, unsigned int y, unsigned int width, unsigned int height, uint16_t** data)
{
    luacomputers::debug("Terminal", "Locking mutex...");
    mutex.lock();
    luacomputers::debug("Terminal", "Flushing buffer...");
    for(int i=y; i<y+height; i++)
    {
        for(int j=x; j<x+width; j++)
        {
            if(j < pix_x && i < pix_y)
            {
                if(data[i-y][j-x]<256)
                {
                    pixels[i][j]->setColor(data[i-y][j-x]);
                }
                //Else: don't change the pixel (transparent)
            }
        }
    }
    luacomputers::debug("Terminal", "Flushed");
    mutex.unlock();
}