#include "UDPSock.hpp"
#include "Debug.hpp"
#include <string>
using namespace luacomputers::networking;

UDPSock::UDPSock() : init(false)
{
	luacomputers::log("UDPSock", "Trying to create an UDP socket with any port...");
	if(socket.bind(sf::Socket::AnyPort) != sf::Socket::Done)
	{
		luacomputers::logError("UDPSock", "failed to create UDP socket.");
	}else
	{
		//Non-blocking mode, returns immediately
		std::string s("Created socket with port ");
		port = socket.getLocalPort();
		s.append(std::to_string(port));
		luacomputers::log("UDPSock", s.c_str());
		socket.setBlocking(false);
		init = true;
	}
	
}

UDPSock::UDPSock(uint16_t port) : init(false)
{
	std::string s("Trying to create an UDP socket with port ");
	s.append(std::to_string(port));
	luacomputers::log("UDPSock", s.c_str());
	if(socket.bind(port) != sf::Socket::Done)
	{
		luacomputers::logError("UDPSock", "failed to create UDP socket.");
	}else
	{
		
		this->port = socket.getLocalPort();
		
		
		//Non-blocking mode, returns immediately
		socket.setBlocking(false);
		init = true;

		std::string s("Created socket with port ");
		s.append(std::to_string(this->port));
		luacomputers::log("UDPSock", s.c_str());

	}
}

UDPSock::~UDPSock()
{
	
}

bool UDPSock::receive()
{
	mut.lock();
	if(socket.receive(packet, sender, senderPort) == sf::Socket::Done)
	{

		if(packet >> data)
		{
			mut.unlock();
			return true;
		}else
		{
			mut.unlock();
			return false;
		}
	}else
	{
		mut.unlock();
		return false;
	}
	
}

bool UDPSock::sendMessage(std::string data, std::string dest, uint16_t port)
{
	mut.lock();
	std::string s("Sending message to: ");
	s.append(dest);
	s.append(":");
	s.append(std::to_string(port));
	luacomputers::log("UDPSock", s.c_str());
	if(data.length() > sf::UdpSocket::MaxDatagramSize)
	{
		std::string s("Couldn't send message to: ");
		s.append(dest);
		s.append(":");
		s.append(std::to_string(port));
		luacomputers::logError("UDPSock", s.c_str());
		mut.unlock();
		return false;
	}
	sf::Packet pack;
	pack << data;
	if(socket.send(pack, sf::IpAddress(dest), port) == sf::Socket::Done)
	{
		std::string s("Sent message to: ");
		s.append(dest);
		s.append(":");
		s.append(std::to_string(port));
		luacomputers::log("UDPSock", s.c_str());
		mut.unlock();
		return true;
	}else
	{
		std::string s("Couldn't send message to: ");
		s.append(dest);
		s.append(":");
		s.append(std::to_string(port));
		luacomputers::logError("UDPSock", s.c_str());
		mut.unlock();
		return false;
	}
	
}

unsigned short UDPSock::getPort()
{
	return port;
}